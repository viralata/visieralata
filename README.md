# Ecran de protectio, COVID 19, à découper à la laser dans du PMMA, focus sur l'optimisation d'utilisation matière

Ce modèle de visière de protection est dérivé du modèle  [Moustache](https://gitlab.com/entraide-maker-covid-19/visieres/-/tree/master/Visi%C3%A8res_d%C3%A9coupe_laser/Mod%C3%A8le_moustache), mais le but est ici d'avoir le moins de perte de matière possible en décomposant la pièce en trois parties. Les branches et la pièce frontale n'ont pas forcément la même épaisseur.
Ce fichier a été testé avec des plaques de PMMA de 3mm d'épaisseur et avec des pièces frontales de 8mm d'épaisseur.
Ce fichier a été créé au fablab de [l'EcoCentre](https://www.varennes-ecocentre.fr/) et testé au fablab [Chantier Libre](https://www.chantierlibre.org/) .

## Montage
Le montage s'effectue à l'aide de deux colliers de serrage nylon (ou colson) de maxi 4mm de large.
Pour les colliers épais il n'est pas nécessaire de trop serrer les colson pour garder un peu de souplesse aux branches pour plus de confort.
Une feuille de plastique A4 de 200µ est utilisée comme écran de protection.
Le percement des feuilles peut se faire à l'aide du gabarit fourni:
* Serrer un paquet de 100 feuilles entre un martyr et le gabarit à l'aide de deux serre-joints.
* Percer avec un forêt les 4 trous
* Deux gabarits sont fournis, un pour des plaques de PMMA de 3mm qui devrrait fonctionner avec des plaques de 2 et 4mm d'&paisseur, un testé avec des plaques de 8mm d'épaisseur.


## Contenu
Vous trouverez dans ce dossier les fichiers suivants:
* Visieralata_1piece.svg comprenant deux branches et une pièce frontale.
* Visieralata_117pieces_100x60.svg comprenant 117 visières calepinées sur une plaque de 100cmx60cm. Si vous avez des plaques de taille différente vous pouvez vous en inspirer pour le calepinage
* Visieralata_Gabarit_ep3mm.svg comprenant un gabarit de percage pour les feuilles transparents si vous utilisez des plaques de 3mm d'épaisseur. Il vous aidera à percer 100 feuilles d'un coup avec un forêt de 6mm
* Visieralata_Gabarit_ep8mm.svg comprenant un gabarit de percage pour les feuilles transparents si vous utilisez des plaques de 8mm d'épaisseur pour la partie frontale et 3mm d'épaisseur pour les branches. Il vous aidera à percer 100 feuilles d'un coup avec un foret de 6mm et un foret de 10mm
